package com.itau.api.cartoes.models;


import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private  Cartao cartao;

    @NotBlank(message ="Descrição do pagamento nao pode ficar em branco.")
    private String descricaodopagamento;

    @DecimalMin(value = "0", message = "Valor do pagamento deve ser maior ou igual a zero")
    @Digits(integer = 6, fraction = 2, message = "Valor do pagamento fora do padrão")
    private double valordopagamento;

    public Pagamento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public double getValordopagamento() {
        return valordopagamento;
    }

    public void setValordopagamento(double valordopagamento) {
        this.valordopagamento = valordopagamento;
    }

    public String getDescricaodopagamento() {
        return descricaodopagamento;
    }

    public void setDescricaodopagamento(String descricaodopagamento) {
        this.descricaodopagamento = descricaodopagamento;
    }
}
