package com.itau.api.cartoes.controllers;

import com.itau.api.cartoes.DTOs.CadastroCartaoDTO;
import com.itau.api.cartoes.DTOs.CadastroPagamentoDTO;
import com.itau.api.cartoes.DTOs.RespostaCadastroCartaoDTO;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.models.Pagamento;
import com.itau.api.cartoes.services.CartaoService;
import com.itau.api.cartoes.services.ClienteService;
import com.itau.api.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/Pagamento")
public class PagamentoController {

    @Autowired
    CartaoService cartaoService;

    @Autowired
    PagamentoService pagamentoService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento cadastrarPagamento (@RequestBody @Valid CadastroPagamentoDTO cadastroPagamentoDTO) {
        try {
            return pagamentoService.cadastrarPagamento(cadastroPagamentoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numeroCartao")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<RespostaCadastroCartaoDTO> consultarTodosCartoes(@PathVariable(name = "numeroCartao") String numerocartao) {

        if (numerocartao != null) {
            return cartaoService.consultarCartaoPorNumero(numerocartao);
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

}
