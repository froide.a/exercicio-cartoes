package com.itau.api.cartoes.controllers;

import com.itau.api.cartoes.DTOs.AtivaDesativaCartaoDTO;
import com.itau.api.cartoes.DTOs.CadastroCartaoDTO;
import com.itau.api.cartoes.DTOs.RespostaCadastroCartaoDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.services.CartaoService;
import com.itau.api.cartoes.services.ClienteService;
import com.itau.api.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaCadastroCartaoDTO cadastrarCartao (@Valid @RequestBody CadastroCartaoDTO cadastroCartaoDTO) {
        try {
            return cartaoService.cadastrarCartao(cadastroCartaoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @GetMapping("/{numeroCartao")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<RespostaCadastroCartaoDTO> consultarTodosCartoes(@PathVariable(name = "numeroCartao") String numerocartao) {

        if (numerocartao != null) {
            return cartaoService.consultarCartaoPorNumero(numerocartao);
            }
          else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
        }

    @GetMapping
    public Iterable<Cartao> lerTodosOsCartoes() {
        return cartaoService.lerTodosOsCartoes();
    }


    //@PatchMapping("/{numeroCartao}")
    //public RespostaCadastroCartaoDTO ativadesativaproduto(@RequestBody @Valid AtivaDesativaCartaoDTO ativaDesativaCartaoDTO, @PathVariable(name = "numerocartao") String numerocartao) {
    //    try {
    //        Cartao cartaoDB = cartaoService.atualizarLimiteCartao(id, selecionaCartaoLimite.getLimite());
    //        return cartaoDB;
    //    } catch (RuntimeException e) {
    //        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
    //    }
    //}

}





