package com.itau.api.cartoes.services;


import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService  {


    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private CartaoService cartaoService;

    //@Autowired
    //private BCryptPasswordEncoder encoder;

    public Cliente cadastrarCliente (Cliente cliente){
        return  clienteRepository.save(cliente);
    }

    public Cliente buscarClientePeloId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return  cliente;
        }else {
            throw new RuntimeException("O cliente não foi encontrado");
        }
    }

    public Iterable<Cliente>lerTodosOsClientes() {
        return clienteRepository.findAll();
    }


   }
