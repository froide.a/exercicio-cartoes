package com.itau.api.cartoes.services;


import com.itau.api.cartoes.DTOs.CadastroCartaoDTO;
import com.itau.api.cartoes.DTOs.RespostaCadastroCartaoDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.repositories.CartaoRepository;
import com.itau.api.cartoes.repositories.ClienteRepository;
import com.itau.api.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import java.time.LocalDate;
import java.util.Random;


@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    ClienteService clienteService;

    @Autowired
    PagamentoService pagamentoService;

    public RespostaCadastroCartaoDTO cadastrarCartao(CadastroCartaoDTO cadastroCartaoDTO) {
        try {

             Cartao cartao = this.converterCadastroCartaoDtoParaCadastro(cadastroCartaoDTO);

            // Obter os dados do cliente
            try {
                Optional<Cliente> cliente = clienteRepository.findById(cadastroCartaoDTO.getIdCliente());

                if (cliente.isPresent()) {
                    cartao.setCliente(cliente.get());
                } else {
                    throw new RuntimeException("Id do cliente não encontrado");
                }
            } catch (RuntimeException e) {
                throw new RuntimeException("Erro ao consultar o cliente por id: " + e.getMessage());
            }
            cartao = cartaoRepository.save(cartao);

            RespostaCadastroCartaoDTO respostaCadastroCartaoDTO = this.converterCartaoParaRespostaCarteiraDto(cartao);

            return respostaCadastroCartaoDTO;

        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao incluir cartao: " + e.getMessage());
        }
    }

    private Cartao converterCadastroCartaoDtoParaCadastro(CadastroCartaoDTO cadastroCartaoDTO) {
        Cartao cartao = new Cartao();
        cartao.setNumeroCartao(cadastroCartaoDTO.getNumeroCartao());
        return cartao;
    }

    private RespostaCadastroCartaoDTO converterCartaoParaRespostaCarteiraDto(Cartao cartao) {
        RespostaCadastroCartaoDTO respostaCadastroCartaoDTO = new RespostaCadastroCartaoDTO();
        respostaCadastroCartaoDTO.setId((cartao.getId()));
        respostaCadastroCartaoDTO.setNumerocartao(cartao.getNumeroCartao());
        //validar
        respostaCadastroCartaoDTO.setIdCliente(cartao.getId());
        //validar
        respostaCadastroCartaoDTO.setAtivo(cartao.isAtivo());

        return respostaCadastroCartaoDTO;
    }

    public List<RespostaCadastroCartaoDTO> consultarCartaoPorNumero(String numeroCartao) {
        try {
            List<Cartao> cartaos = cartaoRepository.findAll();

            if (cartaos.size() > 0) {
                List<RespostaCadastroCartaoDTO> listaRespostaCartaoDTO = new ArrayList<>();
                Cartao cartaoPorNumero = new Cartao();

                cartaoPorNumero = filtrarCartaoPorNumero(numeroCartao, cartaos);

                if (cartaoPorNumero != null) {
                    listaRespostaCartaoDTO .add(this.converterCartaoParaRespostaCarteiraDto(cartaoPorNumero));
                    return listaRespostaCartaoDTO;
                } else {
                    throw new RuntimeException("Cartao não encontrado.");
                }

            } else {
                throw new RuntimeException("Não há Cartões Cadastrados.");
            }
        } catch (RuntimeException e ) {
            throw new RuntimeException("Erro ao consultar o Numero do Cartão: " + e.getMessage());
        }
    }

    private Cartao filtrarCartaoPorNumero(String numeroCartao, List<Cartao> cartaos) {
        for (Cartao cartao : cartaos) {
            if (cartao.getNumeroCartao().equals(numeroCartao)) {
                return cartao;
            }
        }
        return null;
    }

    public Iterable<Cartao>lerTodosOsCartoes() {
        return cartaoRepository.findAll();
    }
}