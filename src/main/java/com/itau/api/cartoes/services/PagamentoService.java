package com.itau.api.cartoes.services;


import com.itau.api.cartoes.DTOs.CadastroCartaoDTO;
import com.itau.api.cartoes.DTOs.CadastroPagamentoDTO;
import com.itau.api.cartoes.DTOs.RespostaCadastroCartaoDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.models.Pagamento;
import com.itau.api.cartoes.repositories.CartaoRepository;
import com.itau.api.cartoes.repositories.ClienteRepository;
import com.itau.api.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    CartaoRepository cartaoRepository;


    public Pagamento cadastrarPagamento(CadastroPagamentoDTO cadastroPagamentoDTO) {
        try {

            Pagamento pagamento = this.converterCadastroPagamentoDtoParaCadastro(cadastroPagamentoDTO);

            // Obter os dados do cartao
            try {
                Optional<Cartao> cartao = cartaoRepository.findById(cadastroPagamentoDTO.getIdCartao());

                if (cartao.isPresent()) {
                    pagamento.setCartao(cartao.get());
                } else {
                    throw new RuntimeException("Id do cliente não encontrado");
                }
            } catch (RuntimeException e) {
                throw new RuntimeException("Erro ao consultar o cliente por id: " + e.getMessage());
            }
            pagamento = pagamentoRepository.save(pagamento);

            //RespostaCadastroCartaoDTO respostaCadastroCartaoDTO = this.converterCartaoParaRespostaCarteiraDto(cartao);

            return pagamento;

        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao incluir cliente: " + e.getMessage());
        }
    }

    private Pagamento converterCadastroPagamentoDtoParaCadastro(CadastroPagamentoDTO cadastroPagamentoDTO) {
        Pagamento pagamento = new Pagamento();
        pagamento.setId(cadastroPagamentoDTO.getIdCartao());
        pagamento.setDescricaodopagamento(cadastroPagamentoDTO.getDescricaodopagamento());
        pagamento.setValordopagamento(cadastroPagamentoDTO.getValordopagamento());
        return pagamento;
    }

}



