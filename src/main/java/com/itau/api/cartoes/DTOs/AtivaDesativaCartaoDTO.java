package com.itau.api.cartoes.DTOs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AtivaDesativaCartaoDTO {
    @NotNull(message =  "Numero do Cartão deve ser preenchido")
    @NotBlank(message = "Numero do Cartão deve ser preenchido")
    private Boolean ativo;

    public AtivaDesativaCartaoDTO() {
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
