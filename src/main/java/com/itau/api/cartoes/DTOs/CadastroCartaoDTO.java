package com.itau.api.cartoes.DTOs;

import javax.validation.constraints.*;

public class CadastroCartaoDTO
{
    @NotNull(message =  "Numero do Cartão deve ser preenchido")
    @NotBlank(message = "Numero do Cartão deve ser preenchido")
    private String numeroCartao;

    @NotNull(message = "Informe o Id do Cliente")
    private int idCliente;

    public CadastroCartaoDTO() {
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
}
