package com.itau.api.cartoes.DTOs;

import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.models.Pagamento;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CadastroPagamentoDTO {

    @NotNull(message = "Informe o Id do Cartao")
    private int IdCartao;

    @NotNull(message =  "Descrição do pagamento deve ser preenchido")
    @NotBlank(message = "Descrição do pagamento deve ser preenchido")
    private String descricaodopagamento;

    @NotNull(message =  "Numero do Cartão deve ser preenchido")
    @NotBlank(message = "Numero do Cartão deve ser preenchido")
    private Double valordopagamento;

    public CadastroPagamentoDTO() {
    }

    public int getIdCartao() {
        return IdCartao;
    }

    public void setIdCartao(int idCartao) {
        IdCartao = idCartao;
    }

    public String getDescricaodopagamento() {
        return descricaodopagamento;
    }

    public void setDescricaodopagamento(String descricaodopagamento) {
        this.descricaodopagamento = descricaodopagamento;
    }

    public Double getValordopagamento() {
        return valordopagamento;
    }

    public void setValordopagamento(Double valordopagamento) {
        this.valordopagamento = valordopagamento;
    }
}


